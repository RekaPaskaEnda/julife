extends Control

onready var cursor = $Cursor

const pos = [
	70,
	190
]

const links = [
	"res://Scenes/Level.tscn",
	"res://Scenes/MainMenu.tscn"
]

var focus = 0
var next_scene

func _ready():
	focus = 0
	set_cursor_position(focus)

func _input(event):
	if event.is_action_pressed('ui_left'):
		focus -= 1
		if focus < 0:
			focus = 1
		set_cursor_position(focus)
	if event.is_action_pressed('ui_right'):
		focus += 1
		if focus > 1:
			focus = 0
		set_cursor_position(focus)
	if event.is_action_pressed('ui_accept'):
		$ButtonAudioPlayer.play()
		next_scene = get_tree().change_scene(str(links[focus]))
		

func set_cursor_position(i):
	cursor.global_position.x = pos[i]
	

