extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var platform = get_parent().get_node("Platform_Area")
	connect("area_entered", platform, "_on_perfect_area_entered_1")
