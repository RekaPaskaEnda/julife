extends Area2D


onready var moving = get_parent().get_node("MovingTween")
onready var platformTween = get_parent()
onready var spawner = get_parent().get_parent().get_node("Spawner")

var spawn = true
var score = true
var perf1 = false
var perf2 = false

func _on_perfect_area_entered_1(area):
	if(area.name == "Feet"):
		perf1 = true
		
func _on_perfect_area_entered_2(area):
	if(area.name == "Feet"):
		perf2 = true


func _on_Platform_Area_area_entered(area):
	if area.get_name() == "Feet":
		platformTween.moving = false
		if spawn:
			spawner.spawn()
		if score:
			score = false
			if (perf1 == true) and (perf2 == true):
				global.point += 20
				get_parent().get_parent().get_node("platformHitPerfect").play()
				get_parent().get_node("TwentyPoint").set_emitting(true)
				get_parent().get_node("PerfectParticle").set_emitting(true)
			else:
				global.point += 10
				get_parent().get_parent().get_node("platformHit").play()
				get_parent().get_node("TenPoint").set_emitting(true)
				get_parent().get_node("NormalParticle").set_emitting(true)
				
		perf1 = false
		perf2 = false
		#moving.stop(platformTween)
		spawn = false
