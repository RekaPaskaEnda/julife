extends Node2D

const idle = 1.0

var number = 0

onready var start = Vector2(get_parent().position)
onready var move_to = Vector2(1025,get_parent().position.y)
onready var follow = Vector2(get_parent().position)
var speed = 10

onready var platform = $Platform
onready var tween = $MovingTween

func _ready():
	print("parent name : ", get_parent().get_name())
	print("parent position : ", get_parent().position)
	if start.x == 0:
		print("start : ", start)
		print("move_to  : ",move_to)
		_init_tween()
	else:
		start.x = 1025
		move_to.x = 0
		print("start : ", start)
		print("move_to  : ",move_to)
		_init_tween()
	
func _init_tween():
	var duration = move_to.length() / float(speed * 40)
	tween.interpolate_property(self, "follow", start, move_to, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, idle)
	tween.interpolate_property(self, "follow", move_to, start, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, duration + idle)
	tween.start()

func _physics_process(delta):
	if number == 100:
		print("last position : ", platform.position)
		
	var duration = move_to.length() / float(speed * 40)
	platform.position = platform.position.linear_interpolate(follow, 0.075)
	number += 1
