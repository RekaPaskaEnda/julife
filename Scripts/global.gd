extends Node

var player = null
var player_jump_speed = -200
var point = 0
var object_speed = 10
var platform_speed = 80
var gravity = 300
var bestscore = 0 setget set_bestscore
var level = 0
var current_platform_speed = 0

const filepath = "./bestscore.data"

func _ready():
	load_bestscore()

func load_bestscore():
	var file = File.new()
	if !file.file_exists(filepath):
		return
	file.open(filepath, File.READ)
	bestscore = file.get_var()
	file.close()

func save_bestscore():
	var file = File.new()
	file.open(filepath,File.WRITE)
	file.store_var(bestscore)
	file.close()

func set_bestscore(value):
	bestscore = value
	save_bestscore()

func instance_node(node, location, parent):
	var node_instance = node.instance()
	parent.add_child(node_instance)
	node_instance.global_position = location
	return node_instance
