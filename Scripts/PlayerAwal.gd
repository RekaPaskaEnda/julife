extends KinematicBody2D

onready var animationPlayer = $AnimationPlayer
onready var audioPlayer = $AudioStreamPlayer2D

const UP = Vector2(0,-1)

export (int) var speed = 200
export (int) var jump_speed = -800
export (int) var GRAVITY = 3500

var velocity = Vector2()

func _ready():
	global.player = self
	animationPlayer.play("idle")

func get_input():
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
		audioPlayer.play()
	
func _physics_process(delta):
	if not is_on_floor():
		animationPlayer.play("jump")
	else:
		animationPlayer.play("idle")
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

