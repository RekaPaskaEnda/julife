extends LinkButton

export(String) var scene_to_load

func _on_StoryBoard_pressed():
	var mainmenu = get_tree().get_root().get_node("MainMenu")
	mainmenu.get_node("ButtonAudioStream").play()
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
