extends Control

var next_level

onready var cursor = $Cursor

const pos = [
	100,
	130
]

var focus = 0
var next_scene

func _ready():
	focus = 0
	set_cursor_position(focus)

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = !get_tree().paused
		visible = get_tree().paused
		
func lanjut():
	get_tree().paused = false
	set_visible(false)
	
func keluar():
	get_tree().paused = false
	next_level = get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
	
func _input(event):
	if event.is_action_pressed('ui_up'):
		focus -= 1
		if focus < 0:
			focus = 1
		set_cursor_position(focus)
	if event.is_action_pressed('ui_down'):
		focus += 1
		if focus > 1:
			focus = 0
		set_cursor_position(focus)
	if event.is_action_pressed('ui_accept'):
		$ButtonAudioPlayer.play()
		if focus == 0:
			get_tree().paused = false
			set_visible(false)
		else:
			get_tree().paused = false
			next_level = get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
	
	
func set_cursor_position(i):
	cursor.global_position.y = pos[i]
