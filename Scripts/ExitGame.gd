extends LinkButton

func _on_ExitGame_pressed():
	var mainmenu = get_tree().get_root().get_node("MainMenu")
	mainmenu.get_node("ButtonAudioStream").play()
	get_tree().quit()
