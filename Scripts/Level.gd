extends Node2D

var smp = preload("res://Assets/Visual/background/bg_smp.png")
var sma = preload("res://Assets/Visual/background/bg_sma.png")
var kuliah = preload("res://Assets/Visual/background/bg-kuliah.png")
var kerja = preload("res://Assets/Visual/background/bg-kerja.png")
var tua = preload("res://Assets/Visual/background/bg-tua.png")
onready var bg1 = $ParallaxBackground/ParallaxLayer/bg1
onready var bg2 = $ParallaxBackground3/ParallaxLayer/bg2
onready var bg3 = $ParallaxBackground2/ParallaxLayer/bg3
var new_level = false
var number = 100

func _ready():
	BGMawal.stop()
	global.level = 1
	global.point = 0
	global.player_jump_speed = -200
	global.gravity = 300
	global.platform_speed = 80
	bg1.set_texture(smp)
	bg2.set_texture(smp)
	bg3.set_texture(smp)

func _process(delta):
	if global.point - number >= 0 and global.point != 0:
		if global.level != 5:
			global.level += 1
			new_level = true
			number += 100
	if new_level:
		if global.level == 2:
			get_node("bgm").stop()
			get_node("upgrade").play()
			get_node("bgm-sma").play()
			bg1.set_texture(sma)
			bg2.set_texture(sma)
			bg3.set_texture(sma)
			new_level = false
			global.platform_speed = 105
		elif global.level == 3:
			get_node("bgm-sma").stop()
			get_node("upgrade").play()
			get_node("bgm-kuliah").play()
			bg1.set_texture(kuliah)
			bg2.set_texture(kuliah)
			bg3.set_texture(kuliah)
			new_level = false
			global.platform_speed = 125
		elif global.level == 4:
			get_node("bgm-kuliah").stop()
			get_node("upgrade").play()
			get_node("bgm-kerja").play()
			bg1.set_texture(kerja)
			bg2.set_texture(kerja)
			bg3.set_texture(kerja)
			new_level = false
			global.platform_speed = 150
		elif global.level == 5:
			get_node("bgm-kerja").stop()
			get_node("upgrade").play()
			get_node("bgm-tua").play()
			bg1.set_texture(tua)
			bg2.set_texture(tua)
			bg3.set_texture(tua)
			new_level = false
