extends TextureButton

func _process(delta):
	if is_pressed() or Input.is_action_just_pressed("pause"):
		get_tree().paused = !get_tree().paused
		visible = get_tree().paused
