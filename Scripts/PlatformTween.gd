extends RigidBody2D

const idle = 1.0

var pf1 = preload("res://Assets/Visual/png_1x/platform_1.png")
var pf2 = preload("res://Assets/Visual/png_1x/platform_2.png")
var pf3 = preload("res://Assets/Visual/png_1x/platform_3.png")
var pf4 = preload("res://Assets/Visual/png_1x/platform_4.png")

onready var start = Vector2(0, position.y)
onready var move_to = Vector2(320,position.y)
onready var follow = Vector2(0,position.y)

onready var player = get_parent().get_node("Player")

onready var tween = $MovingTween
onready var gambarpf = $Sprite

var right = true
var moving = true

func _ready():
	pass
	#if start.x != 0:
		#start.x = 320
		#move_to.x = 0
	#_init_tween()
	
func _init_tween():
	var duration = move_to.length() / float(global.platform_speed * 40)
	tween.interpolate_property(self, "position", start, move_to, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, idle)
	tween.interpolate_property(self, "position", move_to, start, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, duration + idle)
	tween.start()

func _process(_delta):
	if global.level == 2: 
		gambarpf.set_texture(pf1)
	elif global.level == 3: 
		gambarpf.set_texture(pf2)
	elif global.level == 4: 
		gambarpf.set_texture(pf3)
	elif global.level == 5: 
		gambarpf.set_texture(pf4)
	if moving:
		if position.x >= 320:
			right = false
		if position.x <=0:
			right = true
		if !right:
			position.x -= global.platform_speed * _delta
		elif right:
			position.x += global.platform_speed * _delta
		if abs(player.global_position.y - global_position.y) >= 260:
			queue_free()

func position (delta):
	position.x = delta.x
	position.y = delta.y
