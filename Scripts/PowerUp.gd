extends Sprite

export (int) var speed = 50

var direction = true
var velocity = Vector2()
var sprite_name

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if direction == true:
		self.global_position.x += speed * delta
	else:
		self.global_position.x -= speed * delta
	check_position()
	
func check_position():
	if self.global_position.x <= 0:
		direction = true
		self.flip_h = false
		
	if self.global_position.x >= 320:
		direction = false
		self.flip_h = true
		
	if self.global_position.y > (global.player.global_position.y + 200):
		queue_free()


func _on_Powerup_Area_body_entered(body):
	if body.get_name() == "Player":
		
		if sprite_name == "Teacher":
			global.point += 15
			get_parent().get_node("pickup").play()
			get_parent().get_node("guruaudio").play()
			get_parent().get_node("Player").get_node("Plus15").set_emitting(true)
			
		elif sprite_name == "Game":
			get_parent().get_node("fail").play()
			get_parent().get_node("gameaudio").play()
			get_parent().get_node("Player").get_node("Minus10Particle").set_emitting(true)
			if global.point < 10:
				global.point = 0
			else:
				global.point -= 10
				
		elif sprite_name == "Coffee":
			get_parent().get_node("pickup").play()
			get_parent().get_node("kopiaudio").play()
			get_parent().get_node("Player").get_node("SlowDownParticle").set_emitting(true)
			print("pplatform : ", global.platform_speed)
			var speed = global.platform_speed
			if global.current_platform_speed == 0:
				global.current_platform_speed = speed
			global.platform_speed = global.current_platform_speed * 0.5
			print("current platform speed: ", global.current_platform_speed)
			print("platform speed: ", global.platform_speed)
			global.player.start_effect_timer("Coffee")
			
		elif sprite_name == "Boots":
			get_parent().get_node("pickup").play()
			get_parent().get_node("sepatuaudio").play()
			get_parent().get_node("Player").get_node("JumpHeightParticle").set_emitting(true)
			global.player_jump_speed = -250
			global.gravity = 250
			global.player.start_effect_timer("Boots")
		
		queue_free()

