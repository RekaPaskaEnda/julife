extends KinematicBody2D

export (int) var speed = 100

var direction = true
var velocity = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	check_position()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if direction == true:
		self.global_position.x += speed * delta
	else:
		self.global_position.x -= speed * delta
	check_position()
	
func check_position():
	if self.global_position.x == 0:
		direction = true
		$Sprite.flip_h = true
		
	if self.global_position.x == 320:
		direction = false
		$Sprite.flip_h = false

func check_end():
	if direction:
		if self.global_position.x < 0:
			queue_free()
	else:
		if self.global_position.x > 320:
			queue_free()


	if self.global_position.y > (global.player.global_position.y + 200):
		queue_free()


func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		$ImpactTimer.start()


func _on_ImpactTimer_timeout():
	queue_free()
