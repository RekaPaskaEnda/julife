extends Node2D

onready var cursor = $Cursor

const pos = [
	72.161,
	96.126,
	121.353,
	144.057
]

const links = [
	"res://Scenes/StoryboardAwal.tscn",
	"res://Scenes/Storyboard.tscn",
	"res://Scenes/Tutorial.tscn"
]

var focus = 0
var next_scene

func _ready():
	focus = 0
	set_cursor_position(focus)
	BGMawal.stop()

func _input(event):
	if event.is_action_pressed('pause'):
		OS.window_borderless = false
		OS.window_fullscreen = false
	if event.is_action_pressed('ui_up'):
		focus -= 1
		if focus < 0:
			focus = 3
		set_cursor_position(focus)
	if event.is_action_pressed('ui_down'):
		focus += 1
		if focus > 3:
			focus = 0
		set_cursor_position(focus)
	if event.is_action_pressed('ui_accept'):
		$ButtonAudioStream.play()
		print("bestscore" + str(global.bestscore))
		print("focus:" + str(focus))

		if (global.bestscore > 0) and (focus == 0):
			get_tree().change_scene(str("res://Scenes/Level.tscn"))
		elif focus == 3:
			get_tree().quit()
		else:
			next_scene = get_tree().change_scene(str(links[focus]))
		

func set_cursor_position(i):
	cursor.global_position.y = pos[i]
	

