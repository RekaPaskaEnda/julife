extends TextureButton

export(String) var scene_to_load

func _process(delta):
	if is_pressed():
		$AudioStreamPlayer.play()
		get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
