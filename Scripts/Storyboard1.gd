extends Node2D

onready var sprite:Sprite = $Sprite
onready var label:Label = $Label
onready var mainMenu:Button = $MainMenu

const bg0:Texture = preload("res://Assets/Visual/story/story_0.png")
const bg1:Texture = preload("res://Assets/Visual/story/story_1.png")
const bg2:Texture = preload("res://Assets/Visual/story/story_2.png")

const backgrounds = [
	bg0,
	bg1,
	bg1,
	bg2,
	bg2,
	bg2,
	bg2,
	bg2
]

const texts = [
	"Suatu hari Mika memasuki ruangan sang kakak di rumahnya dan melihat lemari buku di dalamnya. Ia menemukan banyak sekali buku peninggalan kakaknya di dalam lemari tersebut.",
	"Mika merasa penasaran dan mengambil salah satu buku yang ada di dalam lemari buku tersebut.",
	"Mika pun mulai membaca buku tersebut, hingga tidak terasa ia berjam-jam duduk disana membaca berbagai buku hingga menjadi tertarik untuk mempelajari banyak hal dalam hidupnya untuk mencapai cita-citanya.",
	"Selama menaiki tangga kehidupannya, Mika menemui berbagai hal yang dapat membuatnya semakin cepat menuju kesuksesannya.",
	"Disaat Mika minum kopi sebelum belajar, ia memiliki fokus yang lebih sehingga waktu belajar nya pun semakin bertambah. Disaat Mika mendapatkan sepatu baru, ia menjadi semakin semangat belajar.",
	"Selain itu, disaat Mika bertemu guru ia pun memiliki pengetahuan yang semakin banyak dan dapat membantunya belajar.", 
	"Namun, terdapat pula hal-hal yang dapat menghalangi Mika sehingga menjerumuskannya menuju kegagalan.",
	"Disaat Mika malah memilih bermain Game, fokus di dalam diri Mika serta waktu belajar nya akan berkurang. Dan juga nyamuk yang mengganggu belajarnya."
]

var current
var current_scene

func _ready():
	current = 0

func _process(_delta):
	sprite.texture = backgrounds[current]
	label.text = texts[current]
	if current == 7:
		mainMenu.visible = true
	else:
		mainMenu.visible = false

func transition(step):
	current += step
	if current < 0 or current > 7:
		current_scene = get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))

func _input(event):
	if event.is_action_pressed("ui_left"):
		transition(-1)	
	if event.is_action_pressed("ui_right"):
		transition(1)

func _on_Back_pressed():
	transition(-1)
	
func _on_Next_pressed():
	transition(1)
