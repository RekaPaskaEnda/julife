extends Node2D

func _input(event):

	if event.is_action_pressed("ui_left"):
		get_tree().change_scene(str("res://Scenes/Tutorial.tscn"))
		
	if event.is_action_pressed("ui_right"):
		get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
