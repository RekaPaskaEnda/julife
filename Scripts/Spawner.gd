extends Position2D

export (PackedScene) var platformScene

onready var platformReference = load (platformScene.get_path())

# Preload scene, to get reference
onready var teacherReference = preload("res://Scenes/Teacher.tscn")
onready var gameReference = preload("res://Scenes/Game.tscn")
onready var coffeeReference = preload("res://Scenes/Coffee.tscn")
onready var bootsReference = preload("res://Scenes/Boots.tscn")
onready var mosquitoReference = preload("res://Scenes/Mosquito.tscn")

# Get node from Level scene
onready var spawner = get_parent().get_node("Spawner")
onready var player = get_parent().get_node("Player")
onready var deadArea = get_parent().get_node("DeadArea")
onready var timeNode = get_node("Timer")

# Constants
const MIN_WAIT_TIME = 3.0
const MAX_WAIT_TIME = 10.0

var random = RandomNumberGenerator.new()
var delay_number = 0
var first_platform = true
var first_power_up = true
var power_number = 0

func _ready():
	randomize()
	spawner.position.y = player.position.y + 2.5
	deadArea.position.y = spawner.position.y + 48

func _physics_process(_delta):
	random.randomize()
	power_number = random.randf_range(0.0,4.0)
	if delay_number == 100:
		spawn()
	delay_number += 1

func spawn():
	var platformInstance = platformReference.instance()
	platformInstance.position(position)
	get_parent().add_child(platformInstance)
	
	if not first_platform:
		first_power_up = false
		deadArea.global_position.y = player.global_position.y + 26
	else:
		first_platform = false

	spawner.position.y -= 48
	if global.level == 5:
		global.platform_speed += rand_range(1,5)

func _on_Timer_timeout():
	if first_power_up:
		pass
	else:
		var power_up_instance
		
		if power_number >= 3.5:
			power_up_instance = mosquitoReference.instance()
			random.randomize()
			var number = random.randf_range(0.0,2.0)
			if number >= 1:
				power_up_instance.position.x = 320
				power_up_instance.position.y = spawner.position.y
				get_parent().add_child(power_up_instance)
			else:
				power_up_instance.position.x = spawner.position.x
				power_up_instance.position.y = spawner.position.y
				get_parent().add_child(power_up_instance)
			return
			
		elif power_number >= 3:
			power_up_instance = bootsReference.instance()
			power_up_instance.sprite_name = "Boots"
			
		elif power_number >= 2:
			power_up_instance = coffeeReference.instance()
			power_up_instance.sprite_name = "Coffee"
			
		elif power_number >= 1:
			power_up_instance = gameReference.instance()
			power_up_instance.sprite_name = "Game"

		elif power_number >= 0:
			power_up_instance = teacherReference.instance()
			power_up_instance.sprite_name = "Teacher"

		power_up_instance.position.x = spawner.position.x
		power_up_instance.position.y = spawner.position.y
		get_parent().add_child(power_up_instance)


	timeNode.set_wait_time(rand_range(MIN_WAIT_TIME, MAX_WAIT_TIME))
	timeNode.start()

