extends Area2D

export (String) var sceneName

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		if global.point > global.bestscore:
			global.set_bestscore(global.point)
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
