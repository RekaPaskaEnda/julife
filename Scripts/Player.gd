extends KinematicBody2D

onready var animationPlayer = $AnimationPlayer
onready var audioPlayer = $AudioStreamPlayer2D

const UP = Vector2(0,-1)

var velocity = Vector2()
var start_x
var powerup = null
var mode

func _ready():
	global.player = self
	start_x = position.x
	animationPlayer.play("idle" + str(global.level))

func get_input():
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = global.player_jump_speed * 1.4
		audioPlayer.play()

func _physics_process(delta):
	if not is_on_floor():
		animationPlayer.play("jump" + str(global.level))
	else:
		animationPlayer.play("idle" + str(global.level))

	if position.x >= start_x + 80 or position.x <= start_x - 80:
		if global.point > global.bestscore:
			global.set_bestscore(global.point)
		get_tree().change_scene(str("res://Scenes/" + "GameOver" + ".tscn"))
		
	velocity.x = 0
	velocity.y += delta * global.gravity * 2
	get_input()
	velocity = move_and_slide(velocity, UP)

func start_effect_timer(effect):
	self.powerup = effect
	if powerup == "Boots":
		$BootsTimer.start()
	if powerup == "Coffee":
		$CoffeeTimer.start()

func _on_CoffeeTimer_timeout():
		global.platform_speed = global.current_platform_speed
		global.current_platform_speed = 0
		print("speed : ",global.platform_speed)

func _on_BootsTimer_timeout():
		global.player_jump_speed = -200
		global.gravity = 300
