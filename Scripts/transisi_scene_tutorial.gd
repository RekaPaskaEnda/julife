extends TextureButton

export(String) var scene_to_load

func _on_Next_pressed(): 
	var audioStream = get_tree().get_root().get_node("ButtonAudioStreamTutorial")
	audioStream.play()
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Back_pressed():
	var audioStream = get_tree().get_root().get_node("ButtonAudioStreamTutorial")
	audioStream.play()
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
