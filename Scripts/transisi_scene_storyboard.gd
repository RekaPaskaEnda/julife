extends LinkButton

export(String) var scene_to_load

func _on_leftscene1_pressed():
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
	
func _on_rightscene1_pressed():
	get_tree().change_scene(str("res://Scenes/Storyboard/Storyboard2.tscn"))
	
func _on_leftscene2_pressed():
	get_tree().change_scene(str("res://Scenes/Storyboard/Storyboard1.tscn"))
	
func _on_rightscene2_pressed():
	get_tree().change_scene(str("res://Scenes/Storyboard/Storyboard3.tscn"))
	
func _on_leftscene3_pressed():
	get_tree().change_scene(str("res://Scenes/Storyboard/Storyboard2.tscn"))
	
func _on_rightscene3_pressed():
	get_tree().change_scene(str("res://Scenes/Storyboard/Storyboard4.tscn"))
	
func _on_leftscene4_pressed():
	get_tree().change_scene(str("res://Scenes/Storyboard/Storyboard3.tscn"))
	
