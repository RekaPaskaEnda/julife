extends LinkButton

export(String) var scene_to_load

var level_exist = false 

func _process(delta):
	if is_pressed():
		if global.bestscore > 0 and self.get_name() == "StartGame":
			$AudioStreamPlayer.play()
			get_tree().change_scene(str("res://Scenes/Level.tscn"))
		else:
			$AudioStreamPlayer.play()
			get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
